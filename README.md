Examples for reference creating games in tic80 with python

Lua to python in tic80:

1. Global variables could need to be declared as global (variable name here) in the needed function
2. Function is called def and it needs : at the end ex: TIC():
3. In an if instead of then use : end is not needed
4. t = t+1 can also be t+=1
5. cls, spr, print, ellif and similar use the same syntax
6. comments are # instead of --
7. You can use python's optional ;
8. elif instead of elseif
9. the ~= operand gives compilation errors use !=
10. for dx=-1,1 do would be: for dx in range (-1,1):
11. import math and import random could be needed in some cases
for example: math.sin(a) or random.randint(a,b)
12. If you get TypeError: expected 'int', got 'float' applt int() where needed
13. If you need the values of mouse() for two variables, first store them like this:
mx = mouse()[0]
my = mouse()[1]
14. If you get TypeError: expected 4 positional arguments, got 0 (clip):
use clip(0, 0, 240, 136) instead of clip()
15. .. concatenates two string you can do it for ex:
print("x+y" + str(m))
16. t:sub(i,i) would be t[i]
17. for i=1,#t do would be:  for i in range(len(t)):
18. Replace math.randomseed with random.seed()
19. Instead of for in pairs use for in enumerate():
20. Instead of a table with {} we can use a list with dictionaries
21. To access the list of dictionaries elements use [] instead of .
22. Replace math.random(a,b) with random.randint(a,b)
23. print(string.format("0x%x = %s",addr, val),5,5,12) would be:
print(str.format(hex(addr)+'='+str(val)),5,5,12)
24. Replace math.random() with: 
random.seed(0)
random.random()
25. Don't store random.random() in a variable, use it directly where needed
26. for i,p in ipairs(particles) would be: for i, p in enumerate(particles):